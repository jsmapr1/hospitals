export function filter(filters, items) {
  // Could be optimized by passing the compare function
  // and using a reducer. For now, keeping things simple.

  return items
    .filter(item => {
      if (filters.get('gender') === 'All') {
        return true;
      }
      return filters.get('gender') === item.gender;
    })
    .filter(item => {
      if (filters.get('distance') === 'All') {
        return true;
      }
      return item.locations[0].distance <= filters.get('distance');
    });
}
