import { filter } from './filter';
describe('filters', () => {
  it('should filter from a map', () => {
    const items = [
      {
        gender: 'Male'
      },
      {
        gender: 'Female'
      }
    ];

    const filters = new Map().set('gender', 'Female').set('distance', 'All');
    expect(filter(filters, items)).toEqual([{ gender: 'Female' }]);
  });

  it('should filter for multiple categories', () => {
    const items = [
      {
        gender: 'Male',
        locations: [
          {
            distance: 5
          }
        ]
      },
      {
        gender: 'Female',
        locations: [
          {
            distance: 5
          }
        ]
      },
      {
        gender: 'Female',
        locations: [
          {
            distance: 10
          }
        ]
      }
    ];

    const result = [
      {
        gender: 'Female',
        locations: [
          {
            distance: 5
          }
        ]
      }
    ];
    const filters = new Map().set('gender', 'Female').set('distance', 5);
    expect(filter(filters, items)).toEqual(result);
  });
});
