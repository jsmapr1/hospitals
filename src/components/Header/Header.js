import React from 'react';
import './Header.css';
import logo from './logo.svg';

function Header() {
  return (
    <div className="header">
      <div className="header__gradient" />
      <div className="header__logo">
        <img src={logo} alt={'logo'} />
      </div>
    </div>
  );
}

export default Header;
