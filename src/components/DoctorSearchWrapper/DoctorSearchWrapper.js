import React, { Component } from 'react';

import { fetchDoctors } from '../../ajax/endpoints';
import ZipCodeSearch from '../ZipCodeSearch/ZipCodeSearch';
import DoctorFilterWrapper from '../DoctorFilterWrapper/DoctorFilterWrapper';

class DoctorSearchWrapperComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctors: [],
      zip: ''
    };
    this.handleSearch = this.handleSearch.bind(this);
  }

  handleSearch(zip) {
    // This would be an ajax call maybe?
    // Going to play pretend and call it here instead of just loading
    // directly during construction.
    fetchDoctors(zip).then(doctors => {
      this.setState({
        doctors,
        zip
      });
    });
    // Would add a catch in here
  }

  render() {
    return (
      <div>
        <ZipCodeSearch handleSearch={this.handleSearch} />
        {this.state.doctors.length > 0 &&
          <DoctorFilterWrapper
            doctors={this.state.doctors}
            zip={this.state.zip}
          />}
      </div>
    );
  }
}

export default DoctorSearchWrapperComponent;
