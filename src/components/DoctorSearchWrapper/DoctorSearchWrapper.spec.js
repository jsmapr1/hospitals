import React from 'react';
import { shallow } from 'enzyme';

import DoctorSearchWrapper from './DoctorSearchWrapper';

describe('DoctorSearchWrapper', () => {
  it('should render DoctorSearchWrapper', () => {
    const wrapper = shallow(<DoctorSearchWrapper />);
    expect(wrapper).toMatchSnapshot();
  });
});
