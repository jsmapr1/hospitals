import React from 'react';
import { shallow } from 'enzyme';

import ZipCodeSearch from './ZipCodeSearch';

describe('ZipCodeSearch', () => {
  it('should render ZipCodeSearch', () => {
    const wrapper = shallow(<ZipCodeSearch />);
    expect(wrapper).toMatchSnapshot();
  });

  it('updates calls sends zip on submit', () => {
    const handleSearchSpy = jest.fn();
    const wrapper = shallow(<ZipCodeSearch handleSearch={handleSearchSpy} />);
    const instance = wrapper.instance();
    const event = {
      target: {
        value: '66049'
      }
    };
    const submit = {
      preventDefault: () => {}
    };
    instance.setZip(event);
    instance.handleSubmit(submit);
    expect(handleSearchSpy).toBeCalledWith('66049');
  });
});
