import React, { Component } from 'react';
import './ZipCodeSearch.css';

class ZipCodeSearchComponent extends Component {
  constructor(props) {
    super(props);
    this.sixDigitsRegEx = '^\\d{5}$';
    this.state = {
      zip: ''
    };
    this.setZip = this.setZip.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { zip } = this.state;
    // Another check. Would also add some clarification text
    // in the off chance that the native check is not working.
    if (zip.match(this.sixDigitsRegEx)) {
      this.props.handleSearch(this.state.zip);
    }
  }

  setZip(e) {
    const zip = e.target.value;
    this.setState({ zip });
  }

  render() {
    return (
      <div className="zip-search">
        <form onSubmit={this.handleSubmit}>
          <label htmlFor="zipSearch">Zip Code</label>
          <input
            id="zipSearch"
            className="zip-search__zip"
            pattern={this.sixDigitsRegEx}
            type="text"
            onChange={this.setZip}
            title="Five Digits"
            required
          />
          <input className="zip-search__submit" type="submit" value="Search" />
        </form>
      </div>
    );
  }
}

export default ZipCodeSearchComponent;
