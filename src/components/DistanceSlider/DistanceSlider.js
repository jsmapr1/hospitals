import React from 'react';

import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import './DistanceSlider.css';

export function DistanceSliderComponent({
  handleSliderChange,
  selectedDistance,
  zip
}) {
  const style = {
    marginTop: 14,
    fontWeight: 'bold'
  };
  return (
    <div className="distance-slider">
      <h2>Distance</h2>
      <Slider
        className="distance-slider__slider"
        min={5}
        max={30}
        step={5}
        defaultValue={30}
        marks={{
          5: { style, label: 5 },
          10: { style, label: 10 },
          15: { style, label: 15 },
          20: { style, label: 20 },
          25: { style, label: 25 },
          30: { style, label: 'All' }
        }}
        trackStyle={{
          backgroundColor: 'inherit',
          margin: 20
        }}
        railStyle={{
          height: 7,
          borderRadius: 0,
          marginLeft: '-1%',
          width: '103%'
        }}
        handleStyle={{
          width: 7,
          height: 25,
          backgroundColor: '#41b5e5',
          marginLeft: -3,
          border: 'none',
          borderRadius: 0
        }}
        dotStyle={{
          display: 'none'
        }}
        onChange={handleSliderChange}
      />
      <div className="distance-slider__current">
        Current: {selectedDistance} miles from {zip}
      </div>
    </div>
  );
}

export default DistanceSliderComponent;
