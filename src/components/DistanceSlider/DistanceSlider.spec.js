import React from 'react';
import { shallow } from 'enzyme';

import DistanceSlider from './DistanceSlider';

describe('DistanceSlider', () => {
  it('should render DistanceSlider', () => {
    const wrapper = shallow(
      <DistanceSlider zip={66049} selectedDistance={20} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
