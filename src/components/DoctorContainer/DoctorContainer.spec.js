import React from 'react';
import { shallow } from 'enzyme';

import DoctorContainer from './DoctorContainer';

describe('DoctorContainer', () => {
  it('should render DoctorContainer', () => {
    const doctors = [
      {
        fullName: 'joe morgan'
      }
    ];
    const wrapper = shallow(<DoctorContainer doctors={doctors} />);
    expect(wrapper).toMatchSnapshot();
  });
});
