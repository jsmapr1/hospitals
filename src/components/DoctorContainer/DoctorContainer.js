import React from 'react';
import './DoctorContainer.css';

import Doctor from '../Doctor/Doctor';

export function DoctorContainerComponent({ doctors, zip }) {
  return (
    <div className="doctor-wrapper">
      <div className="results">
        Total Results for {zip}: {doctors.length}
      </div>
      <div>
        {doctors.map(doctor => {
          return <Doctor doctor={doctor} key={doctor.fullName} />;
        })}
      </div>
    </div>
  );
}

export default DoctorContainerComponent;
