import React from 'react';
import { shallow } from 'enzyme';

import Doctor, { formatDistance } from './Doctor';

describe('Doctor', () => {
  it('should render Doctor', () => {
    const doctor = {
      fullName: 'Nick Andrew Mandela',
      specialties: ['Vein Care Again', 'Neurology'],
      image: '../images/cool.png',
      gender: 'Male',
      locations: [
        {
          name: 'St Louis',
          distance: 0.8122855139943341
        },
        {
          name: 'Vein Care Center',
          distance: 245.59373900731615
        }
      ]
    };
    const wrapper = shallow(<Doctor doctor={doctor} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render Doctor with image if avatar is null', () => {
    const doctor = {
      fullName: 'Nick Andrew Mandela',
      specialties: ['Vein Care Again', 'Neurology'],
      image: null,
      gender: 'Male',
      locations: [
        {
          name: 'St Louis',
          distance: 0.8122855139943341
        },
        {
          name: 'Vein Care Center',
          distance: 245.59373900731615
        }
      ]
    };
    const wrapper = shallow(<Doctor doctor={doctor} />);
    expect(wrapper).toMatchSnapshot();
  });
});

describe('helper functions', () => {
  it('should round and have plural miles', () => {
    expect(formatDistance(2.3)).toEqual('3 miles');
  });

  it('should round and have singular mile for 1', () => {
    expect(formatDistance(0.3)).toEqual('1 mile');
  });
});
