import React from 'react';
import './Doctor.css';

export function formatDistance(distance) {
  const rounded = Math.ceil(distance);
  const text = rounded === 1 ? 'mile' : 'miles';
  return `${rounded} ${text}`;
}
export function DoctorComponent({ doctor }) {
  return (
    <div className="doctor">
      <img
        alt="doctor headshot"
        src={`${doctor.image || '../images/avatar.png'}`}
      />
      <div className="doctor__personal">
        <div className="doctor__name">
          <a href={doctor.url}>
            {doctor.fullName}
          </a>
        </div>
        {doctor.specialties.map(specialty => {
          return (
            <div className="doctor__specialty" key={specialty}>
              {specialty}
            </div>
          );
        })}
      </div>
      <div className="doctor__locations">
        {doctor.locations.map(location => {
          return (
            <div className="doctor__location" key={location.name}>
              <div className="doctor__office">
                <a href={location.url}>
                  {location.name}
                </a>
              </div>
              <div className="doctor__distance">
                {formatDistance(location.distance)}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default DoctorComponent;
