import React, { Component } from 'react';

import Header from '../Header/Header';
import DoctorSearchWrapper from '../DoctorSearchWrapper/DoctorSearchWrapper';
import hospital from './hospital.png';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <div className="main">
          <img className="hero" src={hospital} alt={'hospital'} />
          <DoctorSearchWrapper />
        </div>
      </div>
    );
  }
}

export default App;
