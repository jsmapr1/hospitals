import React from 'react';
import { shallow } from 'enzyme';

import GenderFilter from './GenderFilter';

describe('GenderFilter', () => {
  it('should render GenderFilter', () => {
    const wrapper = shallow(
      <GenderFilter handleGenderChange={() => {}} selectedGender={'All'} />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should change checked based on input', () => {
    const wrapper = shallow(
      <GenderFilter handleGenderChange={() => {}} selectedGender={'Female'} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
