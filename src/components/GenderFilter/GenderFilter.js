import React from 'react';
import './GenderFilter.css';

export function GenderFilterComponent({ handleGenderChange, selectedGender }) {
  return (
    <div>
      <h2>Gender</h2>
      <div className="">
        <input
          type="radio"
          className=""
          id="Male"
          name="gender"
          value="Male"
          onChange={e => {
            handleGenderChange(e.target.value);
          }}
          checked={selectedGender === 'Male'}
        />
        <label htmlFor="Male">
          <span>
            <span />
          </span>
          Male
        </label>
      </div>
      <div className="">
        <input
          type="radio"
          className=""
          id="Female"
          name="gender"
          value="Female"
          onChange={e => {
            handleGenderChange(e.target.value);
          }}
          checked={selectedGender === 'Female'}
        />
        <label htmlFor="Female">
          <span>
            <span />
          </span>
          Female
        </label>
      </div>
      <div className="">
        <input
          type="radio"
          className=""
          id="All"
          name="gender"
          value="All"
          onChange={e => {
            handleGenderChange(e.target.value);
          }}
          checked={selectedGender === 'All'}
        />
        <label htmlFor="All">
          <span>
            <span />
          </span>
          No Preference
        </label>
      </div>
    </div>
  );
}

export default GenderFilterComponent;
