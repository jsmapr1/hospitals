import React from 'react';
import { shallow } from 'enzyme';

import DoctorFilterWrapper from './DoctorFilterWrapper';

describe('DoctorFilterWrapper', () => {
  it('should render DoctorFilterWrapper', () => {
    const doctors = [
      {
        fullName: 'joe morgan'
      }
    ];
    const filters = new Map().set('distance', 'All').set('gender', 'All');

    const wrapper = shallow(
      <DoctorFilterWrapper doctors={doctors} filters={filters} />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('updates distance filter on slider change', () => {
    const doctors = [
      {
        gender: 'Male',
        locations: [
          {
            distance: 5
          }
        ]
      },
      {
        gender: 'Female',
        locations: [
          {
            distance: 5
          }
        ]
      }
    ];

    const wrapper = shallow(<DoctorFilterWrapper doctors={doctors} />);
    const instance = wrapper.instance();
    expect(instance.state.filters.get('distance')).toEqual('All');
    expect(instance.state.filters.get('gender')).toEqual('All');
    expect(wrapper).toMatchSnapshot();
    instance.handleSliderChange(4);
    wrapper.update();
    expect(instance.state.filters.get('distance')).toEqual(4);
    expect(instance.state.filters.get('gender')).toEqual('All');
    expect(wrapper).toMatchSnapshot();
  });
});
