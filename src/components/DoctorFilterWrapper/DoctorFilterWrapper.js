import React, { Component } from 'react';
import './DoctorFilterWrapper.css';

import { filter } from '../../util/filter';
import DistanceSlider from '../DistanceSlider/DistanceSlider';
import GenderFilter from '../GenderFilter/GenderFilter';
import DoctorContainer from '../DoctorContainer/DoctorContainer';

export class DoctorFilterWrapperComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: new Map([['gender', 'All'], ['distance', 'All']])
    };
    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.handleGenderChange = this.handleGenderChange.bind(this);
  }

  handleSliderChange(distance) {
    // would normally put in a config file, but figured keep it here for now
    const MAXIMUM_DISTANCE = 30;
    const normalizedDistance = distance !== MAXIMUM_DISTANCE ? distance : 'All';
    const filters = new Map([
      ...this.state.filters,
      ['distance', normalizedDistance]
    ]);
    this.setState({
      filters
    });
  }

  handleGenderChange(gender) {
    const filters = new Map([...this.state.filters, ['gender', gender]]);
    this.setState({
      filters
    });
  }

  render() {
    const { doctors, zip } = this.props;

    const { filters } = this.state;
    const filteredDoctors = filter(filters, doctors);
    return (
      <div className="results-wrapper">
        <div className="filter-wrapper">
          <DistanceSlider
            handleSliderChange={this.handleSliderChange}
            selectedDistance={filters.get('distance')}
            zip={zip}
          />
          <GenderFilter
            handleGenderChange={this.handleGenderChange}
            selectedGender={filters.get('gender')}
          />
        </div>
        <DoctorContainer doctors={filteredDoctors} zip={zip} />
      </div>
    );
  }
}

export default DoctorFilterWrapperComponent;
