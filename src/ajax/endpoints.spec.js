import { fetchDoctors } from './endpoints';

describe('endpoints', () => {
  it('should return an array of doctors', async () => {
    const doctors = await fetchDoctors(66049);
    expect(Array.isArray(doctors)).toEqual(true);
  });
});
