import doctors from './doctors.json';

export function fetchDoctors(zip) {
  // fetch(zip) and return a promise
  // There are not actually any zip codes in the
  // sample file, so just returning everything.

  return Promise.resolve(doctors.results);
}

export function fetchHospitals() {
  // Other endpoints would go in here
}
