# Hello!
Welcome to my coding test. Please enjoy your visit.

# Starting the app
- `npm install` to get the node_modules, of course.
- `npm start` to get a live example running on
[http://localhost:3000](http://localhost:3000)

# Test
- `npm test`
- If they don't run automatically hit 'a' to run all the tests
- Tests are located along the code they are testing. e.g. `filters.spec.js` will
be in the same location as `filters.js`

# Structure
- __src/ajax__: Fake api calls are here. I usually keep any outside calls in one
place. I hate naming the folder 'ajax' but I keep doing it so it must be good
enough
- __src/components__: Anything React related. Every component has a directory
containing the component, the test, and CSS if needed. Anything name \*Wrapper
is usually a Logic component (smart component, container components, whatever)
- __src/store__: It's not here! This is where I normally put redux stuff, but kept
things light for this project since there was no need to share state among
components outside the hierarchy
- __src/utils__: This is were I functions that transform things and will likely be
used in multiple places.
- __everything else__: Anything not mentioned above is part of `react-create-app`.
It's scaffolding and scripts to run things. I didn't create these and didn't
change any defaults.

# Assumptions and Other Caveats
- Not completely cross browser tested. I don't have internet explorer or Virtual
Machine set up on this computer. If that is crucial to assessment let me know.
Tested on Chrome and Safari. Loads but looks odd on phones (see below).
- There is definitely a different font family, but without photoshop, I don't
know what it is.
- Colors should be fairly close. Found using color picker in chrome.
- Spacing should be fairly close. Found using pixel perfect plugin on chrome.
The mock was not perfectly centered, so it is off by a tiny amount.
- Sometimes the requirements differed from the wireframe, mock. I went with the
requirements in those cases. Exception: on the slider, I just used numbers and
not '5 miles' which would be too cluttered.
- There are no urls for locations. As a result, react won't add an href. Since
is the way it is for every location, I didn't do any checks and assumed the real
data would be accurate
- Not mobile friendly. No media queries or anything like that. The width is %,
but it will not look right on phones.
- No persisted state. You refresh you lose all the things.
- Colors and spacing are guesses since I don't have a copy of Photoshop to open
the psd
- Usually using Airbnb for linting, but decided to just use the default lint
options from `react-create-app` to shorten the amount of time I spend
configuring things.
- No proptypes or flows. Strangely enough, I had a bug today that would never
have happened if we had set the proptype correctly. I'm slowly coming around to
types. Not typescript level, but proptypes are nice.
- Didn't use any CSS preprocessors to avoid the time it would take to set up and
configure. I've used post-css and sass in the past. React-create-app does
the vendor prefix prepocessor, so I didn't include those
- Even though we talked about accessibilty, I didn't do anything special to
check for that.

